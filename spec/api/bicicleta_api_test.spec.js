var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

var base_url = 'http://localhost:3000/api/bicicletas';

describe('Bicicletas API',() => {
    beforeEach( (done) => {      
        //var mongoDB = 'mongodb://localhost/red_bicicletas';
        var mongoDB = 'mongodb+srv://bicicletassoporte:JzRDAa4KNor5ykYL@red-bicicletas.qxhgc.mongodb.net/red_bicicletas?retryWrites=true&w=majority';
        mongoose.set('useUnifiedTopology', true);
        mongoose.set('useCreateIndex', true);
        mongoose.connect(mongoDB,{ useNewUrlParser:true});
        var db = mongoose.connection;
        db.on('error',console.error.bind(console, 'connection error: '));
        db.once('open',() => {
            console.log('coneccion establecida');
            done();
        });
    });

    afterEach( function(done){ 
        Bicicleta.deleteMany({},function(err,success){
            if(err)console.log(err);
            done();
        });
    });

    describe ('GET BICICLETAS /', () => {
        it('Status 200 vacia', (done) => {
            request.get(base_url, function(error, response, body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });        
    });
    
    describe('POST Bicicleta /create', () => {
        it('Status 200', (done) => {
            var headers = {"Content-Type": "application/json"};
            var a = '{ "code": 10, "color": "rojo", "modelo": "urbana", "lat": 12.0978181, "lng": -86.3285019 }';
            
            request.post({
                headers: headers,
                url: base_url + '/create', 
                body: a },
                (error, response, body) => {
                    expect(response.statusCode).toBe(200);

                    var bici = JSON.parse(response.body).bicicleta;

                    expect(bici.color).toBe('rojo');
                    expect(bici.ubicacion[0]).toBe(12.0978181);
                    expect(bici.ubicacion[1]).toBe(-86.3285019);
                    done();
            });
        });
    });

    describe ('POST BICICLETAS /delete', () => {
        it('Status 204', (done) => {
            var headers = {'content-type' : 'application/json'};
            var a = new Bicicleta({code: 10, color: 'rojo', modelo: 'urbana', ubicacion: [-34, -54]});
            var aBiciId = { "id": a.code };
            Bicicleta.add (a);
            
            expect(Bicicleta.allBicis.length).toBe(1);

            request.post({
                headers: headers,
                url: base_url + '/delete',
                body: JSON.stringify(aBiciId)
            }, function(error, response, body) {
                expect(response.statusCode).toBe(204);
                Bicicleta.allBicis(function(err, doc) {
                    expect(doc.length).toBe(0);
                    done();
                });
            });
        });
    });

    describe ('POST BICICLETAS /update', () => {
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var a = new Bicicleta({code: 10, color: 'rojo', modelo: 'urbana', ubicacion: [-34, -54]});
            Bicicleta.add (a, function() {
                var headers = {'content-type' : 'application/json'};
                var updatedBici = { "id": a.code, "color": "verde", "modelo": "montaña", "lat":-33, "lng": -55};
                request.post({
                    headers: headers,
                    url: base_url + '/update',
                    body: JSON.stringify(updatedBici)
                }, function(error, response, body) {
                    expect(response.statusCode).toBe(200);
                    var foundBici = Bicicleta.findByCode(10, function(err, doc) {
                        expect(doc.code).toBe(10);
                        expect(doc.color).toBe(updatedBici.color);
                        expect(doc.modelo).toBe(updatedBici.modelo);
                        expect(doc.ubicacion[0]).toBe(updatedBici.lat);
                        expect(doc.ubicacion[1]).toBe(updatedBici.lng);
                        done();
                    });
                });
            });
        });
    });
});
