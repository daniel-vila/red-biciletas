var Bicicleta = require ('../models/bicicleta');

exports.bicicleta_list = function(req, res){
    Bicicleta.find({}, function (err, bikes) {
        if (err) console.log(err);
        res.render('bicicletas/index', { bicis: bikes });
    });
}

exports.bicicleta_create_get = function(req, res){
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function(req, res){
    Bicicleta.create(
        {
          code: req.body.id,
          color: req.body.color,
          modelo: req.body.modelo,
          ubicacion: [req.body.lat, req.body.lng]
        },
        function (err, bike) {
          if (err) console.log(err);
          res.redirect('/bicicletas');
        }
    );    
}

exports.bicicleta_delete_post = function(req, res){
    Bicicleta.findByIdAndDelete(req.body.id, function (err) {
        if (err) {
          next(err);
        } else {
          res.redirect('/bicicletas');
        }
    });
    /*Bicicleta.removeByCode(req.body.code);

    res.redirect('/bicicletas');*/
}

exports.bicicleta_update_get = function(req, res){
    Bicicleta.findById(req.params.id, function (err, bici) {
        if (err) console.log(err);
        console.log(bici);
        res.render('bicicletas/update', { bici });
    });
}

exports.bicicleta_update_post = function(req, res){
    let updateValues = {
        code: req.body.id,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lng]
    };
    Bicicleta.findByIdAndUpdate(req.params.id, updateValues, function (err, bici) {
      if (err) {
        console.log(err);
        res.render("bicicletas/update", {
          errors: err.errors,
          bici: new Bicicleta({code: req.body.id,
                            color: req.body.color,
                            modelo: req.body.modelo,
                            ubicacion: [req.body.lat, req.body.lng]}),
        });
      } else {
        res.redirect("/bicicletas");
        return;
      }
    });
}